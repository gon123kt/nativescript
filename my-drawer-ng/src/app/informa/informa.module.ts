import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { InformaRoutingModule } from "./informa-routing.module";
import {  AyudaComponent } from "./ayuda/ayuda.component";
import {  ContactoComponent } from "./contacto/contacto.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        InformaRoutingModule
    ],
    declarations: [
        AyudaComponent,
        ContactoComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class InformaModule { }
