import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import {  AyudaComponent } from "./ayuda/ayuda.component";
import {  ContactoComponent } from "./contacto/contacto.component";

const routes: Routes = [
    { path: "contacto", component: ContactoComponent },
    { path: "**", component: AyudaComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class InformaRoutingModule { }

