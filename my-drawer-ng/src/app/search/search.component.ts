import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { NoticiasService } from "../domain/noticias.service";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    //providers: [NoticiasService] // indica si el constructor requiere el injectable
})
export class SearchComponent implements OnInit {

    // se llama al service: noticias
    constructor(private noticias: NoticiasService, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        this.noticias.agregar("Hola!");
        this.noticias.agregar("Hola 2! ");
        this.noticias.agregar("Hola 3!");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(navItemRoute: string): void {
        //console.log(`Index: ${args.index}; View: ${args.view} ; Item: ${this.noticias[args.index]}`);
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }
}
